// ==UserScript==
// @name       Taleo Link Fix
// @namespace  http://www.atlassian.com/
// @version    0.2
// @description  rewrites the links provided to taleo to make them go to the candidate instead of the login screen
// @include    https://mail.google.com/mail*
// @copyright  2011+, Chris Mountford
// ==/UserScript==

// need to transform all links of this form: https://tbe.taleo.net/MANAGER/dispatcher/login.jsp?link=CandidateView&id=12380
// to this https://tbe.taleo.net/NA3/ats/candidates/CandidateView.jsp?entity=CAND&act=show&id=12380

var taleo_link_fix = function() {
  var frames = window.frames;
    for (var f=0; f<frames.length; f++) {
        var allLinks = frames[f].document.links;
        var findBit = "https://tbe.taleo.net/MANAGER/dispatcher/login.jsp?link=CandidateView&id=";
        for (var i=0; i<allLinks.length; i++) {
            //console.log("looking at link " + allLinks[i].href);
            if(allLinks[i].href.length > findBit.length && allLinks[i].href.substr(0, findBit.length) == findBit) {
                console.log("fixing link");
                var id= allLinks[i].href.substr(findBit.length);
                allLinks[i].href = "https://tbe.taleo.net/NA3/ats/candidates/CandidateView.jsp?entity=CAND&act=show&id=" + id;
                allLinks[i].innerHTML = "see candidate";
            }
        }
    }
}

window.addEventListener("load", function(e) {
    taleo_link_fix();
}, false);

GM_registerMenuCommand( "taelo link fix", taleo_link_fix, ";", "alt", "p" );
                       
